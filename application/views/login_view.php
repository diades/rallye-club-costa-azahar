<?php
/**
 * Created by PhpStorm.
 * User: nino
 * Date: 20/5/17
 * Time: 12:50
 */
?>
<!DOCTYPE html>
<html lang="es">

    <head>
        <title>Login</title>
    </head>

    <body>

        <h1>Simple login with Codeigniter</h1>
        <?php echo validation_errors(); ?>
        <?php echo form_open('verifylogin'); ?>
            <label for="username">Usuario:</label>
            <input type="text" size="20" id="username" name="username" />
            <br />
            <label for="password">Password:</label>
            <input type="password" size="20" id="password" name="password" />
            <br/>
            <input type="submit" value="Login" />
        </form>

    </body>

</html>
