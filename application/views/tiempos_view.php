<?php
/**
 * Created by PhpStorm.
 * User: nino
 * Date: 20/5/17
 * Time: 11:09
 */
?>
<!DOCTYPE html>
<html lang = "es">

	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Normalize -->
        <link href="/assets/css/normalize.css" rel="stylesheet">

        <!-- Bootstrap -->
        <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="/assets/css/bootstrap-theme.min.css" rel="stylesheet">

        <!-- jQuery -->
        <script src="/assets/js/jquery-3.2.1.min.js"></script>

        <!-- Bootstrap -->
        <script src="/assets/js/bootstrap.min.js"></script>

        <!-- Javascript -->
        <script type="text/javascript">
            $(document).ready(function(){

                $('ul.tramos li').click(function(ev){
                    ev.preventDefault();

                    get_tiempos();

                    $(this).siblings('li.active').removeClass('active');
                    $('.panel-tiempo.active').removeClass('active');

                    $(this).addClass('active');
                    $('.panel-tiempo[data-tramo="'+$(this).data('tramo')+'"]').addClass('active');
                });

                function get_tiempos() {

                    $.get("/api/tiempos", "", function(data){

                        if (data.general.length) {
                            $(".panel-tiempo[data-tramo='general'] table tbody tr").detach();
                            $.each(data.general, function(key, equipo){
                                var element = $(
                                    "<tr>" +
                                        "<td>" + (key + 1) + "º</td>" +
                                        "<td>" + equipo.equipo_nombre + "<br />" + equipo.coche_nombre + "</td>" +
                                        "<td>" + equipo.total_tiempo + "</td>" +
                                    "</tr>"
                                );
                                $(".panel-tiempo[data-tramo='general'] table tbody").append(element);
                            });
                        }

                        $.each(data.tiempos, function(key_tramo, tramo){
                            if (tramo.length) {
                                $(".panel-tiempo[data-tramo='" + key_tramo + "'] table tbody tr").detach();
                                $.each(tramo, function(key, equipo){
                                    var element = $(
                                        "<tr>" +
                                            "<td>" + (key + 1) + "º</td>" +
                                            "<td>" + equipo.equipo + "<br />" + equipo.coche + "</td>" +
                                            "<td>" + equipo.inicio + "</td>" +
                                            "<td>" + equipo.fin + "</td>" +
                                            "<td>" + equipo.crono + "</td>" +
                                        "</tr>"
                                    );
                                    $(".panel-tiempo[data-tramo='" + key_tramo + "'] table tbody").append(element);
                                });
                            }
                        });
                    }, "JSON");
                }

                get_tiempos();

                setInterval(function(){ get_tiempos(); }, 60000);

            });
        </script>

        <style>
            .panel-tiempo {
                display: none;
            }
            .panel-tiempo.active {
                display: block;
            }
        </style>

		<title>Tiempos</title>
	</head>

	<body>

        <h1>28º Rallye Cerámica</h1>
        <hr />
        <ul class="nav nav-pills tramos">
            <li data-tramo="general" class="active"><a href="#">General</a></li>
            <?php foreach ($tramos as $tramo) : $id = $tramo->rallye_tramo_ID; ?>
               <li data-tramo="<?php echo $id; ?>"><a href="#"><?php echo $tramo->tramo_nombre . ' - ' . $tramo->nombre; ?></a></li>
            <?php endforeach; ?>
        </ul>
        <div>
            <div class="panel-tiempo active" data-tramo="general">
                <h2>Clasificación general</h2>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Pos.</th>
                            <th>Equipo / coche</th>
                            <th>Tiempo</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <?php foreach ($tramos as $tramo) : $id = $tramo->rallye_tramo_ID; $equipos = $tiempos[$id]; ?>
            <div class="panel-tiempo panel-<?php echo $id; ?>" data-tramo="<?php echo $id; ?>">
                <h2><?php echo $tramo->tramo_nombre . " - " . $tramo->nombre; ?></h2>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Pos.</th>
                            <th>Equipo / coche</th>
                            <th>Inicio</th>
                            <th>Fin</th>
                            <th>Tiempo</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <?php endforeach; ?>
        </div>

	</body>

</html>
