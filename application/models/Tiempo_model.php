<?php

/**
 * Created by PhpStorm.
 * User: nino
 * Date: 20/5/17
 * Time: 22:44
 */

class Tiempo_model extends CI_Model
{
	function get_tramos($rallye_ID)
	{
		$this->db->select('rallye_tramo_ID');
		$this->db->select('rallye_tramo.nombre');
		$this->db->select("tramos.nombre AS `tramo_nombre");
		$this->db->distinct(true);
		$this->db->from('tiempos');
		$this->db->join('rallye_tramo', 'rallye_tramo.ID = tiempos.rallye_tramo_ID', 'left');
		$this->db->join('tramos', 'tramos.ID = rallye_tramo.tramo_ID', 'left');
		$this->db->where('rallye_ID', $rallye_ID);
		$this->db->order_by('rallye_tramo.orden');

		$query = $this->db->get();

		return $query->result();
	}

	function get_tiempos_by_tramo($tramo_ID)
	{
		$this->db->select();
		$this->db->from('vista_tiempos_tramo');
		$this->db->where('rallye_tramo_ID', $tramo_ID);

		$query = $this->db->get();

		return $query->result();
	}

	function get_tiempos_by_rallye($rallye_ID)
	{
		$this->db->select("MAX(`total_tramos`) AS max_tramos");
		$this->db->from("vista_tiempos_rallye");
		$this->db->where("rallye_ID", $rallye_ID);
		$this->db->limit(1);

		$query = $this->db->get();
		$result = $query->result();
		$max_tramos = $result[0]->max_tramos;


		$this->db->select();
		$this->db->from('vista_tiempos_rallye');
		$this->db->where('rallye_ID', $rallye_ID);
		$this->db->where('total_tramos', $max_tramos);
		$this->db->order_by('total_tiempo');

		$query = $this->db->get();
		$result = $query->result();

		return $result;
	}

}