<?php

/**
 * Created by PhpStorm.
 * User: nino
 * Date: 20/5/17
 * Time: 12:38
 */
class User_model extends CI_Model
{

	function login($username, $password)
	{
		$this->db->select('ID, usuario, password');
		$this->db->from('usuarios');
		$this->db->where('usuario', $username);
		$this->db->where('password', sha1($password ));
		$this->db->limit(1);

		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->result();
		} else {
			return false;
		}
	}

	function findAll()
	{
		$this->db->select('ID, usuario');
		$this->db->from('usuarios');

		$query = $this->db->get();

		return $query->result();
	}
}
