<?php
/**
 * Created by PhpStorm.
 * User: nino
 * Date: 20/5/17
 * Time: 1:41
 */

class Test extends CI_Controller
{

	public function index() {
		$this->load->view('test');
	}

	public function hello() {
		echo "This is the hello function.";
	}
}