<?php
/**
 * Created by PhpStorm.
 * User: nino
 * Date: 20/5/17
 * Time: 11:07
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tiempos_controller extends CI_Controller
{

	public function __construct() {
		parent::__construct();
		$this->load->model('tiempo_model', '', TRUE);
	}

	public function index() {
		$tramos = $this->tiempo_model->get_tramos(1);
		$data['tramos'] = $tramos;

		$data['general'] = $this->tiempo_model->get_tiempos_by_rallye(1);

		foreach ($tramos as $tramo) {
			$data['tiempos'][$tramo->rallye_tramo_ID] = $this->tiempo_model->get_tiempos_by_tramo($tramo->rallye_tramo_ID);
		}

		$this->load->helper('url');
		$this->load->view( 'tiempos_view', $data);
	}
}
