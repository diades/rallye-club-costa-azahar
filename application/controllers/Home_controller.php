<?php

/**
 * Created by PhpStorm.
 * User: nino
 * Date: 20/5/17
 * Time: 13:13
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

//session_start(); // we need to cal PHP's session object to access it through CI

class Home_controller extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if ($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];
			$this->load->view('home_view', $data);
		}
		else
		{
			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('home', 'refresh');
	}

}