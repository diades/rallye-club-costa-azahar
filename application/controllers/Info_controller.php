<?php

/**
 * Created by PhpStorm.
 * User: nino
 * Date: 20/5/17
 * Time: 11:37
 */
class Info_controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index() {
		$this->load->view( 'info_view' );
	}
}