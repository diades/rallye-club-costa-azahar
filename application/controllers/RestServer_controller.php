<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH . '/libraries/REST_Controller.php');

final class RestServer_controller extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->helper('url');
    }

    public function tiempos_get()
    {
    	$this->load->model('tiempo_model', '', TRUE);

	    $data['general'] = $this->tiempo_model->get_tiempos_by_rallye(1);

	    $tramos = $this->tiempo_model->get_tramos(1);
	    foreach ($tramos as $tramo) {
            $data['tiempos'][$tramo->rallye_tramo_ID] = $this->tiempo_model->get_tiempos_by_tramo($tramo->rallye_tramo_ID);
        }

    	$this->response($data);
    }
    
}
//End of file applications/controller/RestServer_controller.php