<?php

/**
 * Created by PhpStorm.
 * User: nino
 * Date: 20/5/17
 * Time: 12:48
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login_controller extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->helper(array('form'));
		$this->load->view('login_view');
	}

}