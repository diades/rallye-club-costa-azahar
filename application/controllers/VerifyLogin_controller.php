<?php

/**
 * Created by PhpStorm.
 * User: nino
 * Date: 20/5/17
 * Time: 12:58
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

class VerifyLogin_controller extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model', '', TRUE);
	}

	public function index()
	{
		// This method will have the credentials validation
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');

		if ($this->form_validation->run() == FALSE)
		{
			// Field validation false. User redirect to login page.
			$this->load->view("login_view");
		}
		else
		{
			// Go to private area
			redirect('home', 'refresh');
		}
	}

	function check_database($password)
	{
		// Field validation succeeded. Validate against database
		$username = $this->input->post('username');

		// Query the database
		$result = $this->user_model->login($username, $password);

		if ($result)
		{
			$sess_array = array();
			foreach ($result as $row)
			{
				$sess_array = array(
					'id' => $row->ID,
					'username' => $row->usuario
				);
				$this->session->set_userdata('logged_in', $sess_array);
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('check_database', 'Invalid username or password');
			return FALSE;
		}
	}


}